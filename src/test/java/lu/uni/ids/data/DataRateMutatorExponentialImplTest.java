package lu.uni.ids.data;

import io.jenetics.Chromosome;
import lu.uni.ids.data.mutator.DataRateMutatorMultiplierImpl;
import lu.uni.ids.data.mutator.FactorSequenceArrayImpl;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mcr
 */
public class DataRateMutatorExponentialImplTest {
    
    double[] factors;
    DataRateMutatorMultiplierImpl target;
    
    public DataRateMutatorExponentialImplTest() {
        double[] array = {0.981334264166855,0.5071280640555138,
            0.4707073757253195,0.5709170930505173,0.5942535271802645,
            0.8330832553989285,0.6134770702773076,0.7796885515479879,
            0.5582042715093574,0.9716333914629066,0.5577610313812382,
            0.15472473892597094,0.9305684449174694,0.967750912202071,
            0.5945176543727373,0.41734387683948304,0.28282884190263835,
            0.48239057214948633,0.47935837507943035,0.386380205954309,
            0.07498513024972309,0.9927128895519858,0.5560454334981002,
            0.4230209020400617,0.1628363830684374,0.9852608573444422,
            0.7285306571409234,0.01941117897338751,0.5960729284664833,
            0.5881785689114944,0.28732007428018824,0.6217034084719882,
            0.9313497669765131,0.29558668593482684,0.8549804317495119,
            0.1883000030296862,0.5365313260635334,0.5896228515112653,
            0.6861703853214499,0.7206808203541557,0.45977429375944356,
            0.03252617190106055,0.30974573619336454,0.5303460854730396,
            0.9810566353795577,0.05723974283472988,0.8653928000796239,
            0.004645631501450587,0.6963401164378731,0.8196893296650296,
            0.9769878824077667,0.7479748422901046,0.4651511741759644,
            0.9122548253618347,0.5895781110102172,0.17849995339682867,
            0.8527644712769012,0.1955642734469739,0.26238916282459246,
            0.41086627728817204,0.08634475247182472,0.4656001353523058,
            0.45068412823457205};

        factors = array;
        target = new DataRateMutatorMultiplierImpl(
                new FactorSequenceArrayImpl(array), 6300);
    }

    @Test
    public void testMutate_first() {
        DataRate rate = new DataRate(50, 7);
        
        DataRate expected = new DataRate(50, 7 * (1 + factors[0]));
        
        assertEquals(expected, target.mutate(rate));
    }
    
    @Test
    public void testMutate_secondRate_firstFactor() {
        DataRate firstRate = new DataRate(50, 7);
        DataRate secondRate = new DataRate(110, 5);
        target.mutate(firstRate);
        
        DataRate expected = new DataRate(110, 
                5 * (1 + factors[0]));
        
        
        assertEquals(expected, target.mutate(secondRate));
    }
    
    @Test
    public void testMutate_secondFactor() {
        DataRate firstRate = new DataRate(50, 7);
        DataRate secondRate = new DataRate(120, 5);
        DataRate thirdRate = new DataRate(170, 3);
        target.mutate(firstRate);
        target.mutate(secondRate);

        DataRate expected = new DataRate(170,
                3 * (1 + factors[1]) * (1 + factors[0]));

        assertEquals(expected, target.mutate(thirdRate));
    }
    
}
