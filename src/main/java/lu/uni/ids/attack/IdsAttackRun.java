package lu.uni.ids.attack;

import lu.uni.ids.data.DataRate;
import lu.uni.ids.data.DataRateProvider;
import lu.uni.ids.data.mutator.DataRateMutator;
import lu.uni.ids.settings.TrainingSettings;
import lu.uni.ids.clustering.ClusteringIds;
import lu.uni.util.Assert;

/**
 *
 * IdsAttackRun is a stateless service that runs a given training attack against a given IDS.
 */
public class IdsAttackRun {
    
    private ClusteringIds ids;
    private double objective;
    private DataRateProvider legit;
    private DataRateMutator mutator;
    private TrainingSettings settings;
    
    public IdsAttackRun(TrainingSettings settings,
    		DataRateProvider legit,
            DataRateMutator mutator,
            ClusteringIds ids,
            double objective) {
        
        Assert.notNull(legit);
        Assert.notNull(mutator);
        
        this.legit = legit;
        this.mutator = mutator;
        
        this.ids = ids;
        this.objective = objective;
        this.settings = settings;
    }
    
    public IdsAttackRunResult run() {
    	this.legit = legit.replicate();
    	this.mutator = mutator.replicate();
    	
        Assert.isTrue(legit.hasNext());
        
        DataRateProvider provider = new DataRateProvider(legit);
        
        DataRate legitRate = provider.next();
        long initialTime = legitRate.time();
        
        while (legitRate.time() - initialTime < settings.trainingTime()) {
        	ids.learn(legitRate);
            legitRate = provider.next();
        }
        
        initialTime = legitRate.time();
        
        while (provider.hasNext()) {
            legitRate = provider.next();
            
            
            DataRate fakeRate = mutator.mutate(legitRate);
            
            
            boolean detected = ids.learn(fakeRate);
            
            if (detected || objectiveReached()) {
                return new IdsAttackRunResult(
                        mutator,
                        detected, 
                        legitRate.time() - initialTime, 
                        howFarFromObjective());
            }
        }
        
        return new IdsAttackRunResult(
                mutator, 
                false, 
                legitRate.time() - initialTime, 
                howFarFromObjective());
    }
    
    private boolean objectiveReached() {
        return highestCentroid() >= objective;
    }
    
    private double howFarFromObjective() {
        if (highestCentroid() >= objective) {
            return 0;
        } else {
            return closestDistanceToCentroid(objective);
        }
    }
    
    private double closestDistanceToCentroid(double target) {
        return Math.abs(target - ids.closestCentroid(target));
    }
    
    private double highestCentroid() {
        return ids.highestCentroid();
    }
}
