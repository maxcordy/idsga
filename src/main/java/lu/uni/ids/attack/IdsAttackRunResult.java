package lu.uni.ids.attack;

import lu.uni.ids.data.mutator.DataRateMutator;
import lu.uni.util.Assert;

/**
 *
 * IdsAttackRunResult is an immutable representation of the result of a training attack against an IDS.
 */
public class IdsAttackRunResult {

    private DataRateMutator mutator;
    private boolean detected;
    private double distance;
    private long time;
    
    public IdsAttackRunResult(DataRateMutator mutator, boolean detected, long time, double distance) {
        Assert.isTrue(time >= 0);
        Assert.isTrue(distance >= 0);
        
        this.mutator = mutator;
        this.detected = detected;
        this.time = time;
        this.distance = distance;
    }
    
    public DataRateMutator mutator() {
    	return mutator;
    }
    
    public boolean detected() {
        return detected;
    }
    
    public long time() {
        return time;
    }
    
    public double distance() {
        return distance;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!(o instanceof IdsAttackRunResult)) return false;
        
        IdsAttackRunResult other = (IdsAttackRunResult) o;
        
        return other.detected == this.detected
                && other.time == this.time
                && Double.compare(other.distance, this.distance) == 0;
    }
    
    @Override
    public int hashCode() {
        int result = 13;
        
        result = 17 * result + (detected ? 1 : 0);
        result = 17 * result + Long.hashCode(time);
        result = 17 * result + Double.hashCode(distance);
        
        return result;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("IdsAttackRunResult {");
        builder.append("detected = ");
        builder.append(detected);
        builder.append(", time = ");
        builder.append(time);
        builder.append(", distance = ");
        builder.append(distance);
        builder.append("}");
        
        return builder.toString();
    }
}
