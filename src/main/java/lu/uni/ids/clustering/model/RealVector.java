package lu.uni.ids.clustering.model;

/**
 * Represents a vector in R^n.
 * 
 * @author steve.muller@itrust.lu
 *
 */
public class RealVector implements IVector<Integer> {
	private final double[] _values;

	public RealVector(double[] values) {
		if (values == null)
			throw new IllegalArgumentException();
		_values = values;
	}

	@Override
	public double get(Integer index) throws IllegalArgumentException {
		if (index < 0 || index >= _values.length)
			throw new IllegalArgumentException();
		else
			return _values[index];
	}
}
