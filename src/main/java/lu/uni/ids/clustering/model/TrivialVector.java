package lu.uni.ids.clustering.model;

/**
 * Represents a vector in R^1, so a simple real number.
 * 
 * @author steve.muller@itrust.lu
 *
 */
@SuppressWarnings("rawtypes") // don't need dimension
public class TrivialVector implements IVector {
	private final double _value;

	public TrivialVector(double value) {
		_value = value;
	}

	@Override
	public double get(Object index) throws IllegalArgumentException {
		return _value;
	}

	@Override
	public String toString() {
		return "<" + _value + ">";
	}
}
