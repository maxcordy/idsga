package lu.uni.ids.clustering.model;

import java.util.stream.Stream;

/**
 * Represents R^n, the space of reals.
 * 
 * @author steve.muller@itrust.lu
 *
 */
public class RealSpace implements ISpace<Integer> {
	private final int _size;

	public RealSpace(int size) throws IllegalArgumentException {
		if (size < 0)
			throw new IllegalArgumentException();
		_size = size;
	}

	@Override
	public int getNumberOfDimensions() {
		return _size;
	}

	@Override
	public Iterable<Integer> getDimensions() {
		return Stream.iterate(1, i -> i + 1).limit(_size)::iterator;
	}

}
