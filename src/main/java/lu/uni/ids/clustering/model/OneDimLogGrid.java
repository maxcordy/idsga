package lu.uni.ids.clustering.model;

public class OneDimLogGrid implements IGrid<Integer> {

	private final double _min;
	private final double _max;
	
	public OneDimLogGrid(double min, double max) {
		_min = min;
		_max = max;
	}

	public int[] mapToGrid(IVector<Integer> vector) {
		double v1 = vector.get(0);
		int i1 = (int) Math.round(Math.log(Math.min(_max, Math.max(_min, v1))) / Math.log(2));

		return new int[] { i1 };
	}

}