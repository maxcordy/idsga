package lu.uni.ids.clustering.model;

/**
 * Represents a discrete grid in a multi-dimensional vector space.
 * 
 * @author steve.muller@itrust.lu
 *
 * @param <TDimension> The type that designates the dimension.
 */
public interface IGrid<TDimension> {
	/**
	 * Maps the given vector point onto a cell of the grid.
	 * @return Returns an array of indices that uniquely identify the grid cell.
	 * Each array item represents the index of the cell in one dimension of the vector space.
	 */
	int[] mapToGrid(IVector<TDimension> vector);
}
