package lu.uni.ids.clustering.model;

/**
 * Represents a multi-dimensional vector space.
 * 
 * @author steve.muller@itrust.lu
 *
 * @param <TDimension> The type that designates the dimension.
 */
public interface ISpace<TDimension> {
	/**
	 * Gets the number of dimensions of this space.
	 * @return Returns the number of dimensions.
	 */
	int getNumberOfDimensions();
	
	/**
	 * Iterates over all dimensions of this space.
	 * @return Returns an iterable that iterates over the results.
	 */
	Iterable<TDimension> getDimensions();
}
