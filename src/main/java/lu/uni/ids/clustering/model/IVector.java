package lu.uni.ids.clustering.model;

/**
 * Represents a point in a vector space.
 * 
 * @author steve.muller@itrust.lu
 *
 * @param <TDimension> The type that designates the dimension.
 */
public interface IVector<TDimension> {
	
	/**
	 * Gets the value assigned to to the given dimension of this vector point.
	 * @param dimension The dimension. Must be compatible with the vector space.
	 * @return Returns the value.
	 * @throws IllegalArgumentException Throws an exception if the dimension is out of range. 
	 */
	double get(TDimension dimension) throws IllegalArgumentException;
}
