package lu.uni.ids.clustering.model;

import java.util.HashMap;
import java.util.stream.Collectors;

/**
 * Represents a vector that takes its values from a HashMap.
 * 
 * @author steve.muller@itrust.lu
 *
 */
public class HashmapVector<TDimension> implements IVector<TDimension> {
	private final HashMap<TDimension, Double> _values;

	public HashmapVector(HashMap<TDimension, Double> values) {
		if (values == null)
			throw new IllegalArgumentException();
		_values = values;
	}

	@Override
	public double get(TDimension index) throws IllegalArgumentException {
		if (!_values.containsKey(index))
			throw new IllegalArgumentException();
		else
			return _values.get(index);
	}

	@Override
	public String toString() {
		return "<" + String.join(", ", _values.values().stream().map(i -> i.toString()).collect(Collectors.toList())) + ">";
	}
}
