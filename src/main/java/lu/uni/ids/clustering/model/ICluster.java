package lu.uni.ids.clustering.model;

/**
 * Represents a cluster of data points.
 * 
 * @author steve.muller@itrust.lu
 *
 * @param <TDimension> The dimension of the vector space that contains the data points.
 */
public interface ICluster<TDimension> {
	/**
	 * Computes the density of this cluster.
	 * The density intuitively corresponds to the number of data points contained in cells covered by this cluster (a number which fades out with time).
	 * @return Returns the density, a non-negative number.
	 */
	double getDensity();

	/**
	 * Computes the centroid of this cluster.
	 * The centroid is a weighted average of all data points from all contained cells.
	 * @param space The vector space in which the centroid lives.
	 * @return Returns the centroid as a vector.
	 */
	IVector<TDimension> getCentroid(ISpace<TDimension> space);
}
