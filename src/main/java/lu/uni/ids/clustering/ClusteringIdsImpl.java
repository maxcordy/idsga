package lu.uni.ids.clustering;

import java.util.Date;

import lu.uni.ids.clustering.algorithm.IClusterAlgorithm;
import lu.uni.ids.clustering.model.ICluster;
import lu.uni.ids.clustering.model.ISpace;
import lu.uni.ids.clustering.model.TrivialVector;
import lu.uni.ids.data.DataRate;
import lu.uni.ids.settings.DosSettingsFactory;
import lu.uni.util.Assert;

/**
 *
 * Single-instance implementation of ClusteringIds
 */
public class ClusteringIdsImpl implements ClusteringIds {

    private ISpace<Integer> space;
    private IClusterAlgorithm<Integer> clustering;
    
    public ClusteringIdsImpl(int interval) {
    	
    	space = DosSettingsFactory.oneDimensionalSpace();
    	clustering = DosSettingsFactory.clustering(interval, space, DosSettingsFactory.oneDimensionalLogGrid());
    }
    
    @Override
    public boolean learn(DataRate rate) {
        return clustering.learn(
                (new Date(1000 * rate.time())).toInstant(), // Date takes milliseconds as input
                new TrivialVector(rate.value()));
    }

    @Override
    public double closestCentroid(double target) {
        double closestCentroid = 0;

        for (ICluster<Integer> c : clustering.getClusters()) {
            double centroid = c.getCentroid(space)
                    .get(space.getDimensions().iterator().next());

            if (Math.abs(target - centroid)
                    < Math.abs(target - closestCentroid)) {
                closestCentroid = centroid;
            }
        }

        return closestCentroid;
    }

    @Override
    public double highestCentroid() {
        double highestCentroid = 0;

        for (ICluster<Integer> c : clustering.getClusters()) {
            double centroid = c.getCentroid(space)
                    .get(space.getDimensions().iterator().next());

            if (centroid > highestCentroid) {
                highestCentroid = centroid;
            }
        }
        

        Assert.isTrue(highestCentroid < Double.MAX_VALUE); // Sanity check for overflow
        return highestCentroid;
    }
    
}
