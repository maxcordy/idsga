package lu.uni.ids.clustering;

import lu.uni.ids.data.DataRate;

/**
 *	
 *	ClusteringIds is a mutable representation of a clustering-based intrusion detection system.
 */
public interface ClusteringIds {

    boolean learn(DataRate rate);
    
    double closestCentroid(double target);
    
    double highestCentroid();
}
