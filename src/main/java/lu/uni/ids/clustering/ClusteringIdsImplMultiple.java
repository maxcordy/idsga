package lu.uni.ids.clustering;

import lu.uni.ids.data.DataRate;
import lu.uni.util.Assert;

/**
 * 
 * Multi-instance implementation of ClusteringIds.
 */
public class ClusteringIdsImplMultiple implements ClusteringIds {

    private ClusteringIdsImpl[] instances;
    
    public ClusteringIdsImplMultiple(int[] intervals) {
        Assert.notNull(intervals);
        Assert.isTrue(intervals.length > 0);
        
        instances = new ClusteringIdsImpl[intervals.length];
        
        for(int i = 0; i < intervals.length; i++) {
            instances[i] = new ClusteringIdsImpl(intervals[i]);
            Assert.isFalse(intervals[i] == 0);
        }
    }
    
    @Override
    public boolean learn(DataRate rate) {
        boolean result = false;
        for (ClusteringIdsImpl instance : instances) {
            result = result || instance.learn(rate);
        }
        
        return result;
    }

    @Override
    public double closestCentroid(double target) {
        double result = 0;
        
        for (int i = 1; i < instances.length; i++) {
            result = Math.max(result, instances[i].closestCentroid(target));
        }

        return result;
    }

    @Override
    public double highestCentroid() {
        double result = instances[0].highestCentroid();
        
        for (int i = 1; i < instances.length; i++) {
            result = Math.min(result, instances[i].highestCentroid());
        }
        
        return result;
    }
    
}
