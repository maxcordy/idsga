package lu.uni.ids.clustering.algorithm;

import java.util.ArrayList;
import java.util.HashMap;

import lu.uni.ids.clustering.model.HashmapVector;
import lu.uni.ids.clustering.model.ICluster;
import lu.uni.ids.clustering.model.ISpace;
import lu.uni.ids.clustering.model.IVector;

/**
 * Internal class for DbscanClusteringAlgorithm.
 */
public class DbscanCluster<TDimension> implements ICluster<TDimension> {

	public ArrayList<DbscanCell<TDimension>> cells = new ArrayList<>();

	public DbscanCluster(int id) {
	}

	@Override
	public double getDensity() {
		return this.cells.stream().map(cell -> cell.getDensity()).reduce(0.0, Double::sum);
	}

	@Override
	public IVector<TDimension> getCentroid(ISpace<TDimension> space) {
		HashMap<TDimension, Double> totalMass = new HashMap<>();
		double totalWeight = 0.0;

		for (DbscanCell<TDimension> cell : this.cells) {
			totalWeight += cell.getDensity();
			for (TDimension dim : space.getDimensions())
				totalMass.put(dim, totalMass.getOrDefault(dim, 0.0) + cell.getValue().get(dim));
		}
		
		for (TDimension dim : space.getDimensions())
			totalMass.put(dim, totalMass.getOrDefault(dim, 0.0) / totalWeight);
		
		return new HashmapVector<TDimension>(totalMass);
	}

	/**
	 * Adds a cell to this cluster, and sets the cell's parent cluster to this object.
	 * This method changes the 'cluster' of the given cell.
	 * @param cell The cell.
	 */
	public void addCell(DbscanCell<TDimension> cell) {
		cell.setCluster(this);
		cells.add(cell);
	}

	/**
	 * Removes a cell from this cluster, and unsets the cell's parent cluster.
	 * This method changes the 'cluster' of the given cell.
	 * @param cell The cell.
	 */
	public void removeCell(DbscanCell<TDimension> cell) {
		cell.setCluster(null);
		cells.remove(cell);
	}

	/**
	 * Merges this cluster and all contained cells into the given other cluster.
	 * This method changes the 'cluster' of all contained cells. 
	 * @param other The other cluster.
	 */
	public void mergeIntoCluster(DbscanCluster<TDimension> other) {
		for (DbscanCell<TDimension> cell : this.cells) {
			cell.setCluster(other);
			other.cells.add(cell);
		}
		this.cells.clear();
	}
}
