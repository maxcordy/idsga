package lu.uni.ids.clustering.algorithm;

import java.time.Instant;
import java.util.HashMap;

import lu.uni.ids.clustering.model.HashmapVector;
import lu.uni.ids.clustering.model.ICluster;
import lu.uni.ids.clustering.model.ISpace;
import lu.uni.ids.clustering.model.IVector;

/**
 * Internal class for DbscanClusteringAlgorithm.
 */
public class DbscanCell<TDimension> {

	private final ISettings _settings;
	private final IMaxDensityGetter _maxDensityGetter;
	private final ISpace<TDimension> _space;
	public final int[] Indices;

	private Instant _currentTime = null;
	private double _currentDensity = 0;
	private int _currentLabel = 0; // 0=sparse 1=transitional 2=dense
	private DbscanCluster<TDimension> _currentCluster;
	private IVector<TDimension> _currentValue;

	private DbscanCluster<TDimension> _lastClusteringCluster;
	private int _lastClusteringLabel = 0;
	
	public DbscanCell(ISettings settings, IMaxDensityGetter maxDensityGetter, ISpace<TDimension> space, int[] indices) {
		_settings = settings;
		_maxDensityGetter = maxDensityGetter;
		_space = space;
		Indices = indices;
	}

	public void addDatapoint(Instant time, IVector<TDimension> datapoint) {
		if (_currentTime == null)
			_currentTime = time;

		_currentDensity = _currentDensity * _settings.getExponentialDegradation(_currentTime, time) + 1;

		HashMap<TDimension, Double> newValue = new HashMap<>();
		for (TDimension dim : _space.getDimensions()) {
			if (_currentValue == null)
				newValue.put(dim, datapoint.get(dim));
			else
				newValue.put(dim, _currentValue.get(dim) * _settings.getExponentialDegradation(_currentTime, time) + datapoint.get(dim));
		}
		_currentValue = new HashmapVector<TDimension>(newValue);
		_currentTime = time;
		
		determineLabel();
	}

	public void evolve(Instant time) {
		if (_currentTime == null)
			_currentTime = time;

		_currentDensity *= _settings.getExponentialDegradation(_currentTime, time);

		HashMap<TDimension, Double> newValue = new HashMap<>();
		for (TDimension dim : _space.getDimensions()) {
			if (_currentValue == null)
				newValue.put(dim, 0.);
			else
				newValue.put(dim, _currentValue.get(dim) * _settings.getExponentialDegradation(_currentTime, time));
		}
		_currentValue = new HashmapVector<TDimension>(newValue);
		_currentTime = time;

		determineLabel();
	}

	public DbscanCluster<TDimension> getCluster() {
		return _currentCluster;
	}
	
	public void setCluster(DbscanCluster<TDimension> newCluster) {
		_currentCluster = newCluster;
	}

	public void saveClusterInfo() {
		_lastClusteringCluster = _currentCluster;
		_lastClusteringLabel = _currentLabel;
	}

	public double getDensity() {
		return _currentDensity;
	}

	public IVector<TDimension> getValue() {
		return _currentValue;
	}
	
	public ICluster<TDimension> getLastClusteringCluster() {
		return _lastClusteringCluster;
	}

	public boolean isDense() {
		return _currentLabel == 2;
	}

	private void determineLabel() {
		_currentLabel = _settings.isDense(_currentDensity, _maxDensityGetter.getMaxDensity()) ? 2 : 0;
	}
}
