package lu.uni.ids.clustering.algorithm;

import java.time.Duration;
import java.time.Instant;

public interface ISettings {
	public Duration getClusteringDelay();
	public Duration getClusteringInterval();
	public double getExponentialDegradation(Instant start, Instant now);
	public boolean isDense(double density, double maxDensity);
}
