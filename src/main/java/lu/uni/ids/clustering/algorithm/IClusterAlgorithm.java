package lu.uni.ids.clustering.algorithm;

import java.time.Instant;

import lu.uni.ids.clustering.model.ICluster;
import lu.uni.ids.clustering.model.IVector;

/**
 * Algorithm for computing the clusters of a time series of data points.
 * 
 * @author steve.muller@itrust.lu
 *
 * @param <TDimension> The dimension of the vector space that contains the data points.
 */
public interface IClusterAlgorithm<TDimension> {
	/**
	 * Learns a new data point.
	 * 
	 * @param time The time when the data point was collected. The time MUST NOT be earlier than the time previously passed to this method.
	 * @param datapoint The data point.
	 * @throws IllegalArgumentException Throws an exception if the time is not consistent with previous calls to this method.
	 * @return Returns true iff one or more new clusters have been detected.
	 */
	boolean learn(Instant time, IVector<TDimension> datapoint) throws IllegalArgumentException;
	
	/**
	 * Returns the collection of clusters that has been learned so far.
	 * @return Returns an iterator over all current clusters.
	 */
	Iterable<ICluster<TDimension>> getClusters();
}
