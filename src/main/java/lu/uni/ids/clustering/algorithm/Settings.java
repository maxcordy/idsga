package lu.uni.ids.clustering.algorithm;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class Settings implements ISettings {

	public double DenseThresholdPercentage = 0.1;
	public double Lambda = Math.pow(0.01, 1. / 86400 / 30);
	public Duration ClusteringInterval = Duration.ofMinutes(10);
	public Duration ClusteringDelay =  Duration.ofDays(0);

	public Duration getClusteringDelay() {
		return ClusteringDelay;
	}

	public Duration getClusteringInterval() {
		return ClusteringInterval;
	}

	@Override
	public double getExponentialDegradation(Instant start, Instant now) {
		return Math.pow(Lambda, start.until(now, ChronoUnit.SECONDS));
	}

	@Override
	public boolean isDense(double density, double maxDensity) {
		return density >= DenseThresholdPercentage * Math.max(density, maxDensity);
	}

}
