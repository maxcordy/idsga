package lu.uni.ids.clustering.algorithm;

public interface IMaxDensityGetter {
	public double getMaxDensity();
}
