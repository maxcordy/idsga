package lu.uni.ids.clustering.algorithm;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import lu.uni.ids.clustering.model.ICluster;
import lu.uni.ids.clustering.model.IGrid;
import lu.uni.ids.clustering.model.ISpace;
import lu.uni.ids.clustering.model.IVector;

/**
 * DBSCAN implementation of a clustering algorithm.
 * 
 * @author steve.muller@itrust.lu
 *
 * @param <TDimension> The dimension of the vector space that contains the data points.
 */
public class DbscanClusteringAlgorithm<TDimension> implements IClusterAlgorithm<TDimension>, IMaxDensityGetter {

	private final ISettings _settings;
	private final ISpace<TDimension> _space;
	private final IGrid<TDimension> _grid;
	private double _maxDensity = 0.0;
	private Instant _lastClusteringTime = null;
	private ArrayList<DbscanCluster<TDimension>> _clusters = new ArrayList<>();
	private HashMap<Integer, Object> _cells_structured = new HashMap<>();
	private ArrayList<DbscanCell<TDimension>> _cells_all = new ArrayList<>();
	
	public DbscanClusteringAlgorithm(ISettings settings, ISpace<TDimension> space, IGrid<TDimension> grid) {
		_settings = settings;
		_space = space;
		_grid = grid;
	}

	public boolean learn(Instant time, IVector<TDimension> datapoint) throws IllegalArgumentException {
		// Map datapoint to a cell of the space grid
		int[] indices = _grid.mapToGrid(datapoint);
		DbscanCell<TDimension> cell = getOrCreateCell(indices, true);

		// Add the datapoint to the cell
		cell.addDatapoint(time, datapoint);
		_maxDensity = Math.max(_maxDensity, cell.getDensity());

		// Schedule the first clustering after a delay time
		if (_lastClusteringTime == null)
			_lastClusteringTime = time.minus(_settings.getClusteringInterval()).plus(_settings.getClusteringDelay());

		// From time to time, run the (off-line) clustering
		if (Duration.between(_lastClusteringTime, time).compareTo(_settings.getClusteringInterval()) >= 0) {
			_lastClusteringTime = time;

			// Update all cells to reflect the current time
			_cells_all.forEach(c -> c.evolve(time));

			// Update the clusters
			boolean hasNewClusters = updateClusters();

			// Save the result of the clustering
			_cells_all.forEach(c -> c.saveClusterInfo());
			
			return hasNewClusters;
		}
		else
			return false; // no new clusters when no clustering has been done
	}

	public Iterable<ICluster<TDimension>> getClusters() {
		return _clusters.stream().map(c -> (ICluster<TDimension>)c)::iterator;
	}

	/**
	 * Gets (or creates, if requested) a clustering cell at the given index in the vector space grid.
	 * @param indices The indices of the cell in the grid.
	 * @param storeIfNotExists True if the cell shall be stored in the internal collection, if it was newly created.
	 * @return Returns the found cell, or null if none was found (and createIfNotExists == false).
	 */
	@SuppressWarnings("unchecked")
	private DbscanCell<TDimension> getOrCreateCell(int[] indices, boolean storeIfNotExists) {
		HashMap<Integer, Object> ptr = _cells_structured;
		int n = _space.getNumberOfDimensions();
		for (int i = 0; i < n; i++) {
			int index = indices[i];
			if (ptr.containsKey(index)) {
				if (i == n - 1)
					return (DbscanCell<TDimension>)ptr.get(index);
				else
					ptr = (HashMap<Integer, Object>)ptr.get(index);
			}
			else {
				if (i == n - 1) {
					DbscanCell<TDimension> cell = new DbscanCell<>(_settings, this, _space, indices);
					if (storeIfNotExists) {
						_cells_all.add(cell);
						ptr.put(index, cell);
					}
					return cell;
				}
				else {
					HashMap<Integer, Object> map = new HashMap<>();
					ptr.put(index, map);
					ptr = map;
				}
			}
		}
		// should not get here
		return null;
	}

	/**
	 * Updates the clusters to reflect the current state of the grid.
	 * @return Returns true iff a new cluster has been created.
	 */
	private boolean updateClusters() {
		// By definition, a cluster is a set of cells such that:
		// - its cells are interconnected
		// - its cells are all transitional or dense
		// - its inner cells are dense

		// Outline:
		// 1) Assign every dense cell to a dedicated cluster
		// 2) Try to merge clusters until no longer possible
		// 3) Determine new clusters

		_clusters.clear();

		// (1) Dense cells form the basic clusters
		int ctr = 0;
		for (DbscanCell<TDimension> cell : _cells_all) {
			if (cell.isDense()) {
				// If a cell is dense, assign a new cluster to it
				DbscanCluster<TDimension> cluster = new DbscanCluster<TDimension>(++ctr);
				cluster.addCell(cell);
				_clusters.add(cluster);
			}
			else {
				// Otherwise don't
				cell.setCluster(null);
			}
		}
		

		// (2) Merge clusters
		boolean changed;
		do {
			changed = false;

			// foreach cluster c
			for (DbscanCluster<TDimension> c : (List<DbscanCluster<TDimension>>)_clusters.clone()) {
				// foreach outside grid g of c
				for (DbscanCell<TDimension> g : (ArrayList<DbscanCell<TDimension>>)c.cells.clone()) {
					// foreach neighbouring grid h of g
					for (int d = 0; d < _space.getNumberOfDimensions(); d++)
					for (int step = -1; step <= 1; step += 2) {
						int[] indices = g.Indices.clone();
						indices[d] += step;
						DbscanCell<TDimension> h = getOrCreateCell(indices, false);
						DbscanCluster<TDimension> c_ = h.getCluster(); // c'
						// abort if h is already in c
						if (c_ == c) continue;
						// if h belongs to cluster c'
						if (c_ != null) {
							// if |c| > |c'|
							if (c.cells.size() > c_.cells.size()) {
								c_.mergeIntoCluster(c);
								_clusters.remove(c_);
							}
							else {
								c.mergeIntoCluster(c_);
								_clusters.remove(c);
							}
							// Yes, we changed something
							changed = true;
						}
					}
				}
			}
		}
		// until no change in the cluster labels can be made
		while (changed);

		// (3) 'New' clusters are those where no cells have been previously part of any cluster
		return _clusters.stream().anyMatch(c -> c.cells.stream().allMatch(g -> g.getLastClusteringCluster() == null));
	}

	public double getMaxDensity() {
		return _maxDensity;
	}
}
