package lu.uni.ids.experiments;

import lu.uni.ids.data.DataRateProvider;
import lu.uni.ids.data.mutator.DataRateMutator;
import lu.uni.ids.defense.IdsDefenseGenerator;
import lu.uni.ids.defense.IdsDefenseRun;
import lu.uni.ids.defense.IdsDefenseRunResult;
import lu.uni.ids.defense.IdsDefenseRunResultSet;
import lu.uni.ids.settings.DosSettingsFactory;
import lu.uni.ids.settings.TrainingSettings;
import lu.uni.util.Assert;

import java.util.LinkedHashSet;
import java.util.Set;

import lu.uni.ids.attack.IdsAttackRunResult;
import lu.uni.ids.attack.IdsAttackGenerator;

public class RQ3 {
	
	private DataRateProvider legit;
	private int duration;
	private double objective;
	private TrainingSettings settings;
	
	public RQ3(DataRateProvider legit) {
		this.legit = legit;
		this.duration = DosSettingsFactory.DEFAULT_ATTACK_DURATION;
		this.objective = DosSettingsFactory.DEFAULT_OBJECTIVE;
		this.settings = DosSettingsFactory.trainingSettings();
	}
	  
	
public void runDefenders(int repeat) {

		System.out.println("RQ3: Can the co-evolution process produce increasingly-resilient IDS?");
	
        IdsAttackGenerator attacker;
        IdsAttackRunResult attackResult;
        
        int[] d0 = {10, 20, 40, 80, 160, 320, 640};
        int[] d1 = {919,34,808,780};
        int[] d2 = {1026,111,855,823,775};
        int[] d3 = {853,1214,399};
        int[] d4 = {868,1133,773,1349,774,854};
        int[] d5 = {849,1274,1159,1208};
        int[] d6 = {836,868,1160,1312,322,1032,853};
        int[] d7 = {1423,792,815,1398,1309,1333,782,607};
        
        int[][] intervals = new int[7][];
        intervals[0] = d1;
        intervals[1] = d2;
        intervals[2] = d3;
        intervals[3] = d4;
        intervals[4] = d5;
        intervals[5] = d6;
        intervals[6] = d7;
        

    	boolean[][] detected = new boolean[7][repeat];
    	long[][] times = new long[7][repeat];
        
        for (int i = 0; i < repeat; i++) {
        	

    		System.out.println("\t Generating attack " + (i+1) + "/" + repeat);
        	
        	attacker = new IdsAttackGenerator(settings, d0, legit, duration, objective);
            attackResult = attacker.attack();
        	
            
            for(int j = 0; j < 7; j++) {

        		System.out.println("\t \t Testing defence " + (j+1) + "/" + 7);
        		
            	IdsDefenseRunResult result = (new IdsDefenseRun(settings, legit, attackResult.mutator(), intervals[j], objective)).run();
            	detected[j][i] = result.detected();
            	times[j][i] = result.time();
            	//System.out.println("(" + j + "," + i + ") : " + result.detected());
            	//System.out.println("(" + j + "," + i + ") : " + result.time());
            }
        }

		System.out.print("\t Number of attacks detected by the 7 defences: ");
        for(int j = 0; j < 7; j++) {
        	int nbDetected = 0;
        	for (int i = 0; i < repeat; i++) {
        		if (detected[j][i]) {
        			nbDetected++;
        		}
        	}
    		System.out.print(nbDetected+", ");
        }
    	System.out.println("");
    }
	
	public void run(int repeat, int iterations) {

		
    	Long[][] generationsDefender = new Long[10][repeat];
    	Long[][] generationsAttacker = new Long[10][repeat];
    	Long[][] timeDefence = new Long[10][repeat];
    	Long[][] timeAttack = new Long[10][repeat];
    	Integer[][] size = new Integer[10][repeat];
        
        IdsAttackGenerator attacker;
        IdsAttackRunResult attackResult;
        IdsDefenseRunResultSet defenseResults;
        
        
        
        for (int i = 0; i < repeat; i++) {

        	int[] intervals = {10, 20, 40, 80, 160, 320, 640};
        	
        	
            Set<DataRateMutator> mutators = new LinkedHashSet<>();
        	
            attacker = new IdsAttackGenerator(settings, intervals, legit, duration, objective);
            attackResult = attacker.attack();
            
            for (int k = 0; k < iterations; k++) {
                
                mutators.add(attackResult.mutator());
                
                IdsDefenseGenerator defender = new IdsDefenseGenerator(settings, mutators, legit, duration, objective);
                
                
                defenseResults = defender.defend();

                if (defenseResults.hasFalseNegative()) {
                	System.out.println("Best defender found's result: false negative.");
                	Assert.shouldNeverGetHere();
                }
                else if (defenseResults.allDetected()) {
                	size[k][i] = defenseResults.defender().length;
                	timeDefence[k][i] = defenseResults.averageTime();
                	generationsDefender[k][i] = defender.generations();
                	
                } else {
                	System.out.println(
                			"Best defender found's result: " 
                	+ defenseResults.size() + " attacks reached " + defenseResults.averageDistance() + " (average distance)");
                	Assert.shouldNeverGetHere();
                }
                
                intervals = defenseResults.defender();
                attacker = new IdsAttackGenerator(settings, intervals, legit, duration, objective);
                attackResult = attacker.attack();
                Assert.isFalse(attackResult.detected());
            	timeAttack[k][i] = attackResult.time();
            	generationsAttacker[k][i] = attacker.generations();
            }
        }
        
        for (int k = 0; k < 10; k++) {
            System.out.print((k+1) + ";");
            printArrayAsVector(size[k]);
            printArrayAsVector(timeDefence[k]);
            printArrayAsVector(generationsDefender[k]);
            printArrayAsVector(timeAttack[k]);
            printArrayAsVector(generationsAttacker[k]);
        }
    }
	
	private void printArrayAsVector(Object[] array) {
		System.out.print("[");
		
		for(int i = 0; i < array.length; i++) {
			System.out.print(array[i]);
			if (i == array.length - 1) {
				System.out.println("]");
			} else {
				System.out.print(",");
			}
		}
	}
}
