package lu.uni.ids.experiments;

import lu.uni.ids.data.DataRateProvider;
import lu.uni.ids.settings.DosSettingsFactory;
import lu.uni.ids.settings.TrainingSettings;
import lu.uni.util.Assert;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import lu.uni.ids.attack.IdsAttackRunResult;
import lu.uni.ids.attack.IdsAttackGenerator;

public class RQ1 {
	
	private DataRateProvider legit;
	private long duration;
	private long objective;
	private TrainingSettings settings;
	private static final int maxPower = 7;
	
	public RQ1(DataRateProvider legit) {
		this.legit = legit;
		this.duration = DosSettingsFactory.DEFAULT_ATTACK_DURATION;
		this.objective = DosSettingsFactory.DEFAULT_OBJECTIVE;
		this.settings = DosSettingsFactory.trainingSettings();
	}
	    
	
	public void runSingles(int repeat) {

		System.out.println("RQ1.1: Can we automatically generate successful training attacks for single instance IDS?");
		
		String[] idsIntervals = new String[maxPower + 1];
    	Boolean[] detected = new Boolean[maxPower + 1];
    	Double[][] attempts = new Double[maxPower + 1][repeat];
    	Long[][] time = new Long[maxPower + 1][repeat];
    	
    	for (int i = 0; i <= maxPower; i++) {
    		
			int[] intervals;
			
			intervals = new int[1];
			intervals[0] = (int) (10 * (Math.pow(2, i)));
			idsIntervals[i] = "{" + intervals[0] + "}";
		
			// Checks for false positives; comment to reduce computation time.
			/*IdsDefenseRun blankRun = new IdsDefenseRun(
    			settings,
                legit.replicate(), 
                new DataRateMutatorIdempotent(), 
                intervals,
                objective);
			IdsDefenseRunResult blankRunResult = blankRun.run();
			Assert.isFalse(blankRunResult.detected());*/
			
			IdsAttackGenerator attacker = new IdsAttackGenerator(settings, intervals, legit, duration, objective);
			
			System.out.println("\t Attacking IDS with intervals "+idsIntervals[i]);
			
			detected[i] = false;
			
			for (int z = 0; z < repeat; z++) {
				System.out.println("\t \t Iteration "+(z+1)+"/"+repeat+"...");
				IdsAttackRunResult attackResult = attacker.attack();
			
				// Sanity-check: the attack should succeed
				Assert.isFalse(attackResult.detected());
				Assert.isTrue(attackResult.distance() == 0.0);
			    
				detected[i] = detected[i] || attackResult.detected();
				attempts[i][z] = Double.valueOf(attacker.generations());
				time[i][z] = attackResult.time();
			
			}

			System.out.print("\t \t Number of generations: ");
			printDoubleArrayAsVector(attempts[i]);
			System.out.print("\t \t Attack time: ");
			printLongArrayAsVector(time[i]);
						
			
    	}
    }
	
	private void printDoubleArrayAsVector(Double[] array) {
		System.out.print("[");
		
		for(int i = 0; i < array.length; i++) {
			System.out.print(array[i]);
			if (i == array.length - 1) {
				System.out.println("]");
			} else {
				System.out.print(",");
			}
		}
	}
	
	private void printLongArrayAsVector(Long[] array) {
		System.out.print("[");
		
		for(int i = 0; i < array.length; i++) {
			System.out.print(array[i]);
			if (i == array.length - 1) {
				System.out.println("]");
			} else {
				System.out.print(",");
			}
		}
	}

	public void runTuples(int repeat) {

		System.out.println("RQ1.2: Can we automatically generate successful training attacks for multiple instance IDS? (tuples experiment)");
		
		int[] intervals_1 = {10};
		int[] intervals_2 = {10, 20};
		int[] intervals_3 = {10, 20, 40};
		int[] intervals_4 = {10, 20, 40, 80};
		int[] intervals_5 = {10, 20, 40, 80, 160};
		int[] intervals_6 = {10, 20, 40, 80, 160, 320};
		int[] intervals_7 = {10, 20, 40, 80, 160, 320, 640};

		runTuple(intervals_1, repeat);
		runTuple(intervals_2, repeat);
		runTuple(intervals_3, repeat);
		runTuple(intervals_4, repeat);
		runTuple(intervals_5, repeat);
		runTuple(intervals_6, repeat);
		runTuple(intervals_7, repeat);
	}
	
    private void runTuple(int[] intervals, int repeat) {
    	
    	StringBuilder builder = new StringBuilder("{");
		for(int z = 0; z < intervals.length - 1; z++) {
			intervals[z] = intervals[z];
			builder.append(intervals[z]);
			builder.append(", ");
		}
		intervals[intervals.length - 1] = intervals[intervals.length - 1];
		builder.append(intervals[intervals.length - 1]);
		builder.append("}");
		
		String intervalsAsString = builder.toString();
    	
		Long[] detectionTimes = new Long[repeat];
		Long[] generationsProduced = new Long[repeat];
		
		System.out.println("\t Attacking IDS with intervals " + intervalsAsString);
		
		IdsAttackGenerator attacker = new IdsAttackGenerator(settings, intervals, legit, duration, objective);
		
		for(int z = 0; z < repeat; z++) {
			System.out.println("\t \t Iteration "+(z+1) + "/" + repeat + "...");
    		IdsAttackRunResult attackResult = attacker.attack();
    		Assert.isFalse(attackResult.detected());
    		
    		detectionTimes[z] = attackResult.time();
    		generationsProduced[z] = attacker.generations();
		}
		

		System.out.print("\t \t Number of generations: ");		
		System.out.print("[");
		for (int y = 0; y < repeat - 1; y++) {
			System.out.print(generationsProduced[y]);
			System.out.print(",");
		}
		System.out.print(generationsProduced[repeat - 1]);
		System.out.println("]");
		
		System.out.print("\t \t Attack time: ");
		System.out.print("[");
		for (int y = 0; y < repeat - 1; y++) {
			System.out.print(detectionTimes[y]);
			System.out.print(",");
		}
		System.out.print(detectionTimes[repeat - 1]);
		System.out.println("]");

    }

    public void runPairs(int repeat) {
		System.out.println("RQ1.2: Can we automatically generate successful training attacks for multiple instance IDS? (pairs experiment)");
	       
		int maxPower = 6;

	 	Map<String, Long[]> detectionTimes = new LinkedHashMap<>();
	 	Map<String, Long[]> generationsProduced = new LinkedHashMap<>();
	 	
	 	
	 	for(int i = 0; i <= maxPower; i++) {

	 		for(int j = i + 1; j <= maxPower; j++) {
					
				Set<Integer> set = new LinkedHashSet<>();
				
				set.add((int) (10 * (Math.pow(2, i))));
				set.add((int) (10 * (Math.pow(2, j))));
				
				Integer[] intervalsAsObject = set.toArray(new Integer[1]);
				int[] intervals = new int[intervalsAsObject.length];
				
				
				StringBuilder builder = new StringBuilder("{");
				for(int z = 0; z < intervals.length - 1; z++) {
					intervals[z] = intervalsAsObject[z];
					builder.append(intervals[z]);
					builder.append(", ");
				}
				intervals[intervals.length - 1] = intervalsAsObject[intervals.length - 1];
				builder.append(intervals[intervals.length - 1]);
				builder.append("}");
				
				String intervalsAsString = builder.toString();
				
				if (!detectionTimes.containsKey(intervalsAsString)) { 
					
					detectionTimes.put(intervalsAsString, new Long[repeat]);
					generationsProduced.put(intervalsAsString, new Long[repeat]);
    				
					System.out.println("\t Attacking IDS with intervals " + intervalsAsString);		

					IdsAttackGenerator attacker = new IdsAttackGenerator(settings, intervals, legit, duration, objective);
					
					for(int z = 0; z < repeat; z++) {
						System.out.println("\t \t Iteration "+(z+1) + "/" + repeat + "...");
						
                		IdsAttackRunResult attackResult = attacker.attack();
                		
                		// Sanity check: the attack should succeed
                		Assert.isFalse(attackResult.detected());
        				Assert.isTrue(attackResult.distance() == 0.0);
        				
                		detectionTimes.get(intervalsAsString)[z] = attackResult.time();
                		generationsProduced.get(intervalsAsString)[z] = attacker.generations();
					}

					System.out.print("\t \t Number of generations: ");						
					System.out.print("[");
					for (int y = 0; y < repeat - 1; y++) {
						System.out.print(generationsProduced.get(intervalsAsString)[y]);
						System.out.print(",");
					}
					System.out.print(generationsProduced.get(intervalsAsString)[repeat - 1]);
					System.out.println("]");	
					
					System.out.print("\t \t Attack time: ");		
					System.out.print("[");
					for (int y = 0; y < repeat - 1; y++) {
						System.out.print(detectionTimes.get(intervalsAsString)[y]);
						System.out.print(",");
					}
					System.out.print(detectionTimes.get(intervalsAsString)[repeat - 1]);
					System.out.println("]");
    			}		
			}
	    }    
    }
    
    public void runAll(int repeat) {
	       
    	int maxPower = 6;

    	Map<String, Long[]> detectionTimes = new LinkedHashMap<>();
    	Map<String, Long[]> generationsProduced = new LinkedHashMap<>();
    	
    	
    	for(int i = 0; i <= maxPower; i++) {

    		for(int j = i; j <= maxPower; j++) {
    			
    			for(int k = j; k <= maxPower; k++) {
    				
    				for (int l = k; l <= maxPower; l++) {
    				
    					for(int m = l; m <= maxPower; m++) {
    						
    						for(int n = m; n <= maxPower; n++) {
    							
    							Set<Integer> set = new LinkedHashSet<>();
    							
								set.add((int) (10 * (Math.pow(2, i))));
		        				set.add((int) (10 * (Math.pow(2, j))));
		        				set.add((int) (10 * (Math.pow(2, k))));
		        				set.add((int) (10 * (Math.pow(2, l))));
		        				set.add((int) (10 * (Math.pow(2, m))));
		        				set.add((int) (10 * (Math.pow(2, n))));
		        				
		        				Integer[] intervalsAsObject = set.toArray(new Integer[1]);
		        				int[] intervals = new int[intervalsAsObject.length];
		        				
		        				
		        				StringBuilder builder = new StringBuilder("{");
		        				for(int z = 0; z < intervals.length - 1; z++) {
		        					intervals[z] = intervalsAsObject[z];
		        					builder.append(intervals[z]);
		        					builder.append(", ");
		        				}
		        				intervals[intervals.length - 1] = intervalsAsObject[intervals.length - 1];
		        				builder.append(intervals[intervals.length - 1]);
		        				builder.append("}");
		        				
		        				String intervalsAsString = builder.toString();
		        				
		        				if (!detectionTimes.containsKey(intervalsAsString)) { 
		        					
		        					detectionTimes.put(intervalsAsString, new Long[repeat]);
		        					generationsProduced.put(intervalsAsString, new Long[repeat]);
			        				
		        					System.out.print(intervalsAsString);
		        					

	        						IdsAttackGenerator attacker = new IdsAttackGenerator(settings, intervals, legit, duration, objective);
		        					
		        					for(int z = 0; z < repeat; z++) {
				                		IdsAttackRunResult attackResult = attacker.attack();
				                		
				                		// Sanity check: the attack should succeed
				                		Assert.isFalse(attackResult.detected());
				        				Assert.isTrue(attackResult.distance() == 0.0);
				        				
				                		detectionTimes.get(intervalsAsString)[z] = attackResult.time();
				                		generationsProduced.get(intervalsAsString)[z] = attacker.generations();
		        					}
		        					
		        					System.out.print(";[");
		        					for (int y = 0; y < repeat - 1; y++) {
		        						System.out.print(detectionTimes.get(intervalsAsString)[y]);
		        						System.out.print(",");
		        					}
	        						System.out.print(detectionTimes.get(intervalsAsString)[repeat - 1]);
		        					System.out.print("]");
		        					System.out.print(";[");
		        					for (int y = 0; y < repeat - 1; y++) {
		        						System.out.print(generationsProduced.get(intervalsAsString)[y]);
		        						System.out.print(",");
		        					}
	        						System.out.print(generationsProduced.get(intervalsAsString)[repeat - 1]);
		        					System.out.println("]");
			        			}
		        				
    						}
    					}
    				}
    			}
    		}
    	}    
    }
}
