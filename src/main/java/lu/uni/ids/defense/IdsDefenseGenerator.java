package lu.uni.ids.defense;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import io.jenetics.Chromosome;
import io.jenetics.Genotype;
import io.jenetics.IntegerChromosome;
import io.jenetics.IntegerGene;
import io.jenetics.Optimize;
import io.jenetics.Phenotype;
import io.jenetics.engine.Engine;
import io.jenetics.engine.EvolutionResult;
import io.jenetics.engine.Limits;
import io.jenetics.util.Factory;
import io.jenetics.util.IntRange;
import lu.uni.ids.data.DataRateProvider;
import lu.uni.ids.data.mutator.DataRateMutator;
import lu.uni.ids.data.mutator.DataRateMutatorIdempotent;
import lu.uni.ids.settings.TrainingSettings;
import lu.uni.util.Assert;

/**
 * 
 * IdsDefenseGenerator is a stateful service responsible for generating defenses against training attacks.
 */
public class IdsDefenseGenerator {
    
	private TrainingSettings settings;
    private Set<DataRateMutator> attackers;
    private DataRateProvider legit;
    private Phenotype<IntegerGene, Double> result;
    private double objective;
    private double duration;
    
    private static final int MAX_INTERVAL = 1440;
    private static final int MAX_CLUSTERS = 10;
    private static final int MIN_CLUSTERS = 1;
    private static final int LIMIT = 50;
    
    public IdsDefenseGenerator(TrainingSettings settings, Set<DataRateMutator> mutators, DataRateProvider provider, int duration, double objective) {
    	Assert.notNull(settings);
    	Assert.notNull(mutators);
    	Assert.notEmpty(mutators);
        Assert.notNull(provider);
        Assert.isTrue(objective > 0);
        
        this.attackers = new LinkedHashSet<>();
        for (DataRateMutator mutator : mutators) {
        	this.attackers.add(mutator);
        }
        this.legit = provider;
        this.settings = settings;
        this.duration = duration;
    }
    
    public IdsDefenseGenerator(TrainingSettings settings, DataRateMutator mutator, DataRateProvider provider, int duration, double objective) {
    	Assert.notNull(settings);
    	Assert.notNull(mutator);
        Assert.notNull(provider);
        Assert.isTrue(objective > 0);
        
        this.attackers = new LinkedHashSet<>();
        this.attackers.add(mutator.replicate());
        this.legit = provider;
        this.settings = settings;
        this.objective = objective;
        this.duration = duration;
    }
    
    public IdsDefenseRunResultSet defend() {
        // 1.) Define the genotype (factory) suitable
        //     for the problem.
        Factory<Genotype<IntegerGene>> gtf
                = Genotype.of(IntegerChromosome.of(1, MAX_INTERVAL, IntRange.of(MIN_CLUSTERS, MAX_CLUSTERS)));
        //= Genotype.of(DoubleChromosome.of(0, OBJECTIVE, 17000));

        // 3.) Create the execution environment.
        Engine<IntegerGene, Double> engine = Engine
                .builder(this::eval, gtf)
                .optimize(Optimize.MINIMUM)
                .populationSize(24)
                .build();

        // 4.) Start the execution (evolution) and
        //     collect the result.
        result = engine.stream()
                .limit(Limits.bySteadyFitness(LIMIT))
                .collect(EvolutionResult.toBestPhenotype());

        //System.out.println("Best defender found = " + result.getGenotype());
        //System.out.println("Best defender found's fitness value = " + result.getFitness());
        //System.out.println("Best defender found's generation = " + result.getGeneration());
        
        return evalRun(result.getGenotype());
    }
    
    public long generations() {
    	return result.getGeneration();
    }

        // 2.) Definition of the fitness function.
    private double eval(Genotype<IntegerGene> gt) {
    	
    	IdsDefenseRunResultSet results = evalRun(gt);
    	double eval;

    	double alpha = 10;
    	double beta = 1;
    	
    	if (results.hasFalseNegative()) {
    		eval = 31548480.0;
    		Assert.shouldNeverGetHere();
    	} else if (results.allDetected()) {
    		eval = alpha * (results.averageTime() / 31548480.0) + beta * ((double) results.defender().length) / ((double) MAX_CLUSTERS) - 100.0;
    	} else {
    		eval = alpha * (- results.averageTime() / 31548480.0) + beta * ((double) results.defender().length) / ((double) MAX_CLUSTERS);
    	}
    	
    	return eval;
    }

    private IdsDefenseRunResultSet evalRun(Genotype<IntegerGene> gt) {
    	int[] intervals = intervals(gt.getChromosome());
    	
    	// Comment this to reduce computation cost substantially (risk of false negatives)
    	if (blankRun(intervals).detected()) {
    		return IdsDefenseRunResultSet.buildFalseNegative(intervals);
    	}
    	
    	IdsDefenseRunResultSet results = new IdsDefenseRunResultSet(intervals);
    	
    	for(DataRateMutator attacker : attackers) {
    		IdsDefenseRunResult result = eval(intervals, attacker);
    		results.add(result);
    	}
    	
    	Assert.equals(results.size(), attackers.size());
        
    	return results;
    }
    
    private IdsDefenseRunResult blankRun(int[] intervals) {
    	IdsDefenseRun run = new IdsDefenseRun(
    			settings,
                legit.replicate(), 
                new DataRateMutatorIdempotent(), 
                intervals,
                objective);
        
        return run.run();
    }
    
    private int[] intervals(Chromosome<IntegerGene> chr) {
    	int[] intervals = new int[chr.length()];
    	Iterator<IntegerGene> iterator = chr.iterator();
    	
    	int i = 0;
    	while (iterator.hasNext()) {
    		IntegerGene gene = iterator.next();
    		intervals[i] = gene.intValue(); 
    		Assert.isFalse(intervals[i] == 0);
    		i++;
    	}
    	
    	return intervals;
    }
    
    private IdsDefenseRunResult eval(int[] intervals, DataRateMutator attacker) {
        IdsDefenseRun run = new IdsDefenseRun(
        		settings,
                legit, 
                attacker, 
                intervals, 
                objective);
        
        IdsDefenseRunResult result = run.run();
        
        return result;
    }

}
