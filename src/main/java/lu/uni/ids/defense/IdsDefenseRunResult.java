package lu.uni.ids.defense;

import java.util.Arrays;
import lu.uni.util.Assert;

/**
 *
 * IdsDefenseRunResult is an immutable representation of the result of defending against a given training attack.
 */
public class IdsDefenseRunResult {

    private int[] intervals;
    private boolean detected;
    private long time;
    private double distance;
    
    public IdsDefenseRunResult(int[] intervals, boolean detected, long time, double distance) {
        Assert.isTrue(time >= 0);
        Assert.isTrue(distance < Double.MAX_VALUE);
        
        this.intervals = Arrays.copyOf(intervals, intervals.length);
        this.detected = detected;
        this.time = time;
        this.distance = distance;
    }
    
    public int[] intervals() {
    	return Arrays.copyOf(intervals, intervals.length);
    }
    
    public boolean detected() {
        return detected;
    }
    
    public long time() {
        return time;
    }
    
    public double distance() {
    	return distance;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!(o instanceof IdsDefenseRunResult)) return false;
        
        IdsDefenseRunResult other = (IdsDefenseRunResult) o;
        
        return other.detected == this.detected
                && other.time == this.time
                && other.distance == this.distance
                && Arrays.equals(other.intervals, intervals);
    }
    
    @Override
    public int hashCode() {
        int result = 13;
        
        result = 17 * result + (detected ? 1 : 0);
        result = 17 * result + Long.hashCode(time);
        result = 17 * result + Double.hashCode(distance);
        
        for(int i = 0; i < intervals.length; i++) {
        	result = 17 * result + intervals[i];
        }
        
        return result;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("IdsDefenseRunResult {");

        builder.append("defender = ");
        builder.append(Arrays.toString(intervals));
        builder.append(", detected = ");
        builder.append(detected);
        builder.append(", time = ");
        builder.append(time);
        builder.append(", highestCentroid = ");
        builder.append(distance);
        builder.append("}");
        
        return builder.toString();
    }
}
