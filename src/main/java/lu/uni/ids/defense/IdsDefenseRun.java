package lu.uni.ids.defense;

import lu.uni.ids.data.DataRate;
import lu.uni.ids.data.DataRateProvider;
import lu.uni.ids.data.mutator.DataRateMutator;
import lu.uni.ids.settings.TrainingSettings;

import java.util.Arrays;

import lu.uni.ids.clustering.ClusteringIds;
import lu.uni.ids.clustering.ClusteringIdsImplMultiple;
import lu.uni.util.Assert;

/**
 *
 * IdsDefenseRun is a stateless service that runs a given IDS defense against a given training attack.
 */
public class IdsDefenseRun {
    
    private DataRateProvider legit;
    private DataRateMutator mutator;
    private int[] intervals;
    private TrainingSettings settings;
    private double objective;
    private ClusteringIds ids;
    
    
    public IdsDefenseRun(TrainingSettings settings,
    		DataRateProvider legit, 
            DataRateMutator mutator, 
            int[] intervals,
            double objective) {
        
        Assert.notNull(legit);
        Assert.notNull(mutator);
        Assert.isTrue(objective > 0);
        
        this.legit = legit;
        this.mutator = mutator;
        this.intervals = Arrays.copyOf(intervals, intervals.length);
        this.settings = settings;
        this.objective = objective;
    }
    
    public IdsDefenseRunResult run() {
        this.legit = legit.replicate();
        this.mutator = mutator.replicate();
        ids = new ClusteringIdsImplMultiple(intervals);
        
        Assert.isTrue(legit.hasNext());
        
        DataRateProvider provider = new DataRateProvider(legit);
        DataRate legitRate = provider.next();
        long initialTime = legitRate.time();
        
        while (legitRate.time() - initialTime < settings.trainingTime()) {
            ids.learn(legitRate);
            legitRate = provider.next();
        }
        
        initialTime = legitRate.time();
        
        while (provider.hasNext()) {
            legitRate = provider.next();
            
            DataRate fakeRate = mutator.mutate(legitRate);
            
            boolean detected = ids.learn(fakeRate);
            
            if (detected || objectiveReached()) {
                return new IdsDefenseRunResult(
                        intervals,
                        detected, 
                        legitRate.time() - initialTime,
                        howFarFromObjective());
            }
        }
        
        return new IdsDefenseRunResult(
                intervals, 
                false, 
                legitRate.time(),
                howFarFromObjective());
    }
    
    
    private boolean objectiveReached() {
        return highestCentroid() >= objective;
    }
    
    private double howFarFromObjective() {
        if (highestCentroid() >= objective) {
            return 0;
        } else {
            return closestDistanceToCentroid(objective);
        }
    }
    
    private double closestDistanceToCentroid(double target) {
        return Math.abs(target - ids.closestCentroid(target));
    }
    
    private double highestCentroid() {
        return ids.highestCentroid();
    }
}
