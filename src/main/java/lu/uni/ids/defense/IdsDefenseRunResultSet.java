package lu.uni.ids.defense;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import lu.uni.util.Assert;

/**
 * 
 * IdsDefenseRunResult is an mutable representation of the results of defending against a set of given training attacks.
 */
public class IdsDefenseRunResultSet {

	private List<IdsDefenseRunResult> results;
	private int[] intervals;
	private boolean falseNegative;
	
	public IdsDefenseRunResultSet(int[] intervals) {
		results = new LinkedList<>();
		this.intervals = Arrays.copyOf(intervals, intervals.length);
		this.falseNegative = false;
	}
	
	public static IdsDefenseRunResultSet buildFalseNegative(int[] intervals) {
		IdsDefenseRunResultSet resultSet = new IdsDefenseRunResultSet(intervals);
		
		resultSet.falseNegative();
		
		return resultSet;
	}
	
	private void falseNegative() {
		this.falseNegative = true;
	}
	
	public boolean hasFalseNegative() {
		return falseNegative;
	}
	
	public void add(IdsDefenseRunResult result) {
		Assert.isFalse(falseNegative);
		results.add(result);
	}
	
	public boolean allDetected() {
		Assert.isFalse(falseNegative);
		Assert.isTrue(results.size() > 0);
		
		boolean detected = true;
		
		for (IdsDefenseRunResult result : results) {
			detected = detected && result.detected(); 
		}
		
		return detected;
	}
	
	public long averageTime() {
		Assert.isFalse(falseNegative);
		Assert.isTrue(results.size() > 0);
		
		long totalTime = 0;
		
		for (IdsDefenseRunResult result : results) {
			totalTime = totalTime + result.time(); 
		}
		
		return totalTime / results.size();
	}
	
	public double averageDistance() {
		Assert.isFalse(falseNegative);
		Assert.isTrue(results.size() > 0);
		
		double total = 0;
		
		for (IdsDefenseRunResult result : results) {
			if (!result.detected()) {
				total = total + result.distance(); 
			}
		}
		
		return total / results.size();
	}
	
	public int[] defender() {
		return Arrays.copyOf(intervals, intervals.length);
	}
	
	public String defenderAsString() {
		StringBuilder builder = new StringBuilder("[");
		
		for (int i = 0; i < intervals.length; i++) {
			builder.append(intervals[i]);
			builder.append(",");
		}
		
		builder.setCharAt(builder.length() - 1, ']');
		
		return builder.toString();
	}
	
	public int size() {
		return results.size();
	}
	
}
