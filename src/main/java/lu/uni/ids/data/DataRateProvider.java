package lu.uni.ids.data;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import lu.uni.util.Assert;

/**
 * 
 * DataRateProvider is a stateful services providing timed sequences of network traffic data.
 */
public class DataRateProvider implements Iterator<DataRate> {
    
    public static final long TIME_INCREMENT = 60;
    
    private final List<DataRate> rates;
    private final long firstInstanceTime;
    private final int repeat;
    
    private int remainingRepeat;
    private int current;
    private long timeOffset;
    
    public DataRateProvider(List<DataRate> rates) {
        this(rates, 0);
    }
    
    public DataRateProvider(List<DataRate> rates, int repeat) {
        
        Assert.notNull(rates);
        Assert.notEmpty(rates);
        
        this.repeat = repeat;
        this.remainingRepeat = repeat;
        this.timeOffset = 0;
        this.firstInstanceTime = rates.get(0).time();
        this.current = 0;
        this.rates = new LinkedList<>();

        for (DataRate rate : rates) {
        	Assert.isTrue(rate.value() < 1000.0);
            this.rates.add(rate);
        }
    }
    
    public DataRateProvider(DataRateProvider copy) {
        this(copy.rates, copy.repeat);
    }
    
    @Override
    public boolean hasNext() {
        return size() > 0;
    }
    
    public int size() {
        return rates.size() - current + rates.size() * remainingRepeat;
    }

    @Override
    public DataRate next() {
        DataRate result;
        
        DataRate rate = rates.get(current);
        current++;
        
        result = new DataRate(rate.time() + timeOffset, rate.value());
        
        if (current == rates.size() && remainingRepeat > 0) {
            remainingRepeat--;
            current = 0;
            timeOffset = timeOffset
                    + rates.get(rates.size() - 1).time()
                    - firstInstanceTime
                    + TIME_INCREMENT;

        }
        
        return result;
    }
    
    public DataRateProvider replicate() {
    	DataRateProvider result = new DataRateProvider(this);
    	
    	Assert.isTrue(this.rates.size() == result.rates.size());
    	
    	return result;
    }
    
}