package lu.uni.ids.data;

import lu.uni.util.Assert;

/**
 * 
 * DataRate is an immutable representation of a network traffic data.
 */
public class DataRate {

    private long time;
    private double value;
    
    
    public DataRate(long time, double value) {
        Assert.isTrue(time > 0);
        Assert.isTrue(value >= 0);
        
        this.time = time;
        this.value = value;
    }
    
    public long time() {
        return time;
    }
    
    public double value() {
        return value;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!(o instanceof DataRate)) return false;
        
        DataRate other = (DataRate) o;
        
        return other.value == this.value && other.time == this.time;
    }
    
    @Override
    public int hashCode() {
        int result = 13;
        result = result * 17 + Double.hashCode(value);
        result = result * 17 + Long.hashCode(time);
        
        return result * 17;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("DataRate { ");
        
        builder.append("time = ");
        builder.append(time);
        builder.append(", value = ");
        builder.append(value);
        builder.append("}");
        
        return builder.toString();
    }
}
