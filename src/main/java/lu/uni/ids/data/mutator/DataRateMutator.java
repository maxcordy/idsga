package lu.uni.ids.data.mutator;

import lu.uni.ids.data.DataRate;

/**
 * 
 * DataRateMutator is a stateful service for mutating/altering network traffic data.
 */
public interface DataRateMutator {
    
    DataRate mutate(DataRate rate);
    
    DataRateMutator replicate();
    
}
