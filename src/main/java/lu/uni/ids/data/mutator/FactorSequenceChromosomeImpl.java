package lu.uni.ids.data.mutator;

import io.jenetics.Chromosome;
import io.jenetics.DoubleGene;
import java.util.Iterator;
import lu.uni.util.Assert;

/**
 *
 * Chromosome-based implementation of factor sequence.
 */
public class FactorSequenceChromosomeImpl implements FactorSequence {

    private final Chromosome<DoubleGene> chromosome;
    private Iterator<DoubleGene> chrIterator;
    
    public FactorSequenceChromosomeImpl(Chromosome<DoubleGene> chr) {
        Assert.notNull(chr);
        
        this.chromosome = chr;
        this.chrIterator = this.chromosome.iterator();
        Assert.isTrue(chrIterator.hasNext());
    }

    @Override
    public int length() {
       return chromosome.length();
    }

    @Override
    public double next() {
        return chrIterator.next().doubleValue();
    }

    @Override
    public void reinit() {
        this.chrIterator = this.chromosome.iterator();
    }

    @Override
    public boolean hasNext() {
        return chrIterator.hasNext();
    }
    
    @Override
    public FactorSequence replicate() {
    	return new FactorSequenceChromosomeImpl(chromosome);
    }
}
