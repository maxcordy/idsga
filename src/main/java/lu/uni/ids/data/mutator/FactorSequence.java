package lu.uni.ids.data.mutator;

/**
 * 
 * FactorSequence is a stateful service allowing the iteration over sequences of factors.
 */
public interface FactorSequence {
    
    int length();
    boolean hasNext();
    double next();
    void reinit();
    FactorSequence replicate();
    
}
