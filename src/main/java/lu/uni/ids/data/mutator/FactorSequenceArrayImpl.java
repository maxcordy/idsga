package lu.uni.ids.data.mutator;

import lu.uni.util.Assert;

/**
 *
 * Array-based implementation of FactorSequence
 */
public class FactorSequenceArrayImpl implements FactorSequence {

    private double[] array;
    private int index;
    
    public FactorSequenceArrayImpl(double[] array) {
        Assert.notNull(array);
        
        this.array = array;
        this.index = 0;
    }
    
    @Override
    public int length() {
        return array.length;
    }

    @Override
    public boolean hasNext() {
        return index < array.length;
    }

    @Override
    public double next() {
        double result = array[index];
        
        index++;
        
        return result;
    }

    @Override
    public void reinit() {
        index = 0;
    }
    
    @Override
    public FactorSequence replicate() {
    	return new FactorSequenceArrayImpl(array);
    }
}
