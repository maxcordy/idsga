package lu.uni.ids.data.mutator;

import lu.uni.ids.data.DataRate;

/**
 *	Implementation of DataRateMutator that does not alter the original data.
 */
public class DataRateMutatorIdempotent implements DataRateMutator {

	@Override
	public DataRate mutate(DataRate rate) {
		return rate;
	}

	@Override
	public DataRateMutator replicate() {
		return this;
	}
}
