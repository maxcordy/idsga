package lu.uni.ids.data.mutator;

import lu.uni.ids.data.DataRate;
import lu.uni.util.Assert;

/**
 * 
 * Implementation of DataRateMutator based on applying increasing multiplying factors over time.
 */
public class DataRateMutatorMultiplierImpl implements DataRateMutator {

    private final FactorSequence factors;
    private final long factorDuration;
    private final long duration;
    
    
    private double currentFactor;
    private long currentDuration;
    private long currentTime;
    
    public DataRateMutatorMultiplierImpl(FactorSequence factors, long duration) {
        Assert.notNull(factors);
        
        this.factors = factors;
        this.duration = duration;
        this.factorDuration = Math.floorDiv(duration, factors.length());
        Assert.isTrue(factors.hasNext());
        
        this.currentFactor = 1.0 + factors.next();
        this.currentDuration = this.factorDuration;
        this.currentTime = 0;
    }
    
    @Override
    public DataRate mutate(DataRate rate) {
        long elapsed = rate.time() - currentTime;
        double value = rate.value() * currentFactor;
        
        currentTime = rate.time();
        
        currentDuration = currentDuration - elapsed;
        if (currentDuration <= 0) {
            if (!factors.hasNext()) {
                factors.reinit();
            }
            currentFactor = currentFactor * (1.0 + factors.next());
            currentDuration = factorDuration;
        }
        
        return new DataRate(rate.time(), value);
    }

    @Override
    public DataRateMutator replicate() {
        return new DataRateMutatorMultiplierImpl(factors.replicate(), duration);
    }
    
}
