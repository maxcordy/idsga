package lu.uni.ids.data.mutator;

import io.jenetics.Chromosome;
import io.jenetics.DoubleGene;
import java.util.Iterator;

import lu.uni.ids.data.DataRate;
import lu.uni.util.Assert;

/**
 *
 * Implementation of DataRateMutator based on directly specifying the exact value of the altered data rate.
 */
public class DataRateMutatorDirectValueImpl implements DataRateMutator {

    private Chromosome<DoubleGene> chromosome;
    private Iterator<DoubleGene> fakeDataIterator;
    
    public DataRateMutatorDirectValueImpl(Chromosome<DoubleGene> chromosome) {
        Assert.notNull(chromosome);
        
        this.chromosome = chromosome;
        this.fakeDataIterator = chromosome.iterator();
        
        Assert.isTrue(fakeDataIterator.hasNext());
    }

    @Override
    public DataRate mutate(DataRate rate) {
        
        if (!fakeDataIterator.hasNext()) {
            fakeDataIterator = chromosome.iterator();
        }
        
        double fakeValue = fakeDataIterator.next().doubleValue();
        
        return new DataRate(rate.time(), fakeValue);
    }

    @Override
    public DataRateMutator replicate() {
        return new DataRateMutatorDirectValueImpl(chromosome);
    }
}