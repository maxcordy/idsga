package lu.uni.ids.settings;

/**
 * 
 * TrainingSettings is an immutable representation of clustering-based IDS training settings.
 */
public class TrainingSettings {

	private long trainingTime;
	
	public TrainingSettings(long trainingTimeInSeconds) {
		this.trainingTime = trainingTimeInSeconds;
	}
	
	public long trainingTime() {
		return trainingTime;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		
		if (!(o instanceof TrainingSettings)) {
			return false;
		}
		
		TrainingSettings other = (TrainingSettings) o;
		
		return this.trainingTime == other.trainingTime;
	}
	
	@Override
	public int hashCode() {
		int result = 17;
		
		result = result * Long.valueOf(trainingTime).intValue() + 13;
		
		return result;
	}
}
