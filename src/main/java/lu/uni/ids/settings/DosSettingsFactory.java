package lu.uni.ids.settings;

import java.time.Duration;

import lu.uni.ids.clustering.algorithm.DbscanClusteringAlgorithm;
import lu.uni.ids.clustering.algorithm.IClusterAlgorithm;
import lu.uni.ids.clustering.algorithm.Settings;
import lu.uni.ids.clustering.model.IGrid;
import lu.uni.ids.clustering.model.ISpace;
import lu.uni.ids.clustering.model.OneDimLogGrid;
import lu.uni.ids.clustering.model.RealSpace;

/**
 * 
 * Factory of Settings for DDoS attack detection systems.
 */
public class DosSettingsFactory {
	
	public static final int DEFAULT_TRAINING_TIME = 60 * 60 * 24 * 30;
	public static final int DEFAULT_ATTACK_DURATION = 31548480;
	public static final int DEFAULT_OBJECTIVE = 50000; 
	
	public static TrainingSettings trainingSettings() {
		return new TrainingSettings(DEFAULT_TRAINING_TIME);
	}
	
	public static TrainingSettings trainingSettings(int time) {
		return new TrainingSettings(time);
	}
	
	public static IClusterAlgorithm<Integer> clustering(int intervalInMinutes, ISpace<Integer> space, IGrid<Integer> grid) {
	    Settings settings = new Settings();
	    settings.Lambda = Math.pow(0.01, 1. / 86400); // data will be persistent for ~30 days
        settings.ClusteringInterval = Duration.ofMinutes(intervalInMinutes); // off-line clustering happens every 'interval' min

        return new DbscanClusteringAlgorithm<>(settings, space, grid);
	}
	
	public static ISpace<Integer> oneDimensionalSpace() {
		return new RealSpace(1);
	}
	
	public static IGrid<Integer> oneDimensionalLogGrid() {
		return new OneDimLogGrid(128, Math.pow(2, 20));
	}

}
